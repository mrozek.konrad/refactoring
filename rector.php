<?php

declare(strict_types=1);

use Rector\CodingStyle\Rector\ClassConst\VarConstantCommentRector;
use Rector\CodingStyle\Rector\ClassMethod\UnSpreadOperatorRector;
use Rector\CodingStyle\Rector\FuncCall\PreslashSimpleFunctionRector;
use Rector\CodingStyle\Rector\Use_\RemoveUnusedAliasRector;
use Rector\Core\Configuration\Option;
use Rector\DeadCode\Rector\ClassMethod\RemoveUnusedConstructorParamRector;
use Rector\DeadCode\Rector\Concat\RemoveConcatAutocastRector;
use Rector\Naming\Rector\ClassMethod\MakeGetterClassMethodNameStartWithGetRector;
use Rector\Naming\Rector\ClassMethod\RenameParamToMatchTypeRector;
use Rector\Privatization\Rector\Class_\ChangeReadOnlyVariableWithDefaultValueToConstantRector;
use Rector\Set\ValueObject\SetList;
use Rector\TypeDeclaration\Rector\ClassMethod\AddArrayParamDocTypeRector;
use Rector\TypeDeclaration\Rector\ClassMethod\AddArrayReturnDocTypeRector;
use Rector\TypeDeclaration\Rector\FunctionLike\ReturnTypeDeclarationRector;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    // get parameters
    $parameters = $containerConfigurator->parameters();

    // Define what rule sets will be applied
    $parameters->set(
        Option::SETS,
        [
            SetList::DEAD_CODE,
            SetList::ACTION_INJECTION_TO_CONSTRUCTOR_INJECTION,
            SetList::ARRAY_STR_FUNCTIONS_TO_STATIC_CALL,
            SetList::CARBON_2,
            SetList::CODE_QUALITY,
            SetList::CODE_QUALITY_STRICT,
            SetList::CODING_STYLE,
            SetList::DEAD_DOC_BLOCK,
            SetList::DEAD_CODE,
            SetList::DEFLUENT,
            SetList::EARLY_RETURN,
            SetList::NAMING,
            SetList::ORDER,
            SetList::PHP_74,
            SetList::PHP_80,
            SetList::PHPUNIT_90,
            SetList::PHPUNIT_91,
            SetList::PRIVATIZATION,
            SetList::PSR_4,
            SetList::TYPE_DECLARATION,
        ]
    );

    $parameters->set(
        Option::SKIP,
        [
            ChangeReadOnlyVariableWithDefaultValueToConstantRector::class,
            RemoveUnusedConstructorParamRector::class,
            MakeGetterClassMethodNameStartWithGetRector::class,
            VarConstantCommentRector::class,
            AddArrayReturnDocTypeRector::class,
            AddArrayParamDocTypeRector::class,
            UnSpreadOperatorRector::class,
            RenameParamToMatchTypeRector::class,
            RemoveConcatAutocastRector::class,
            ReturnTypeDeclarationRector::class,
        ]
    );

    $services = $containerConfigurator->services();
    $services->set(PreslashSimpleFunctionRector::class);
    $services->set(RemoveUnusedAliasRector::class);
};
