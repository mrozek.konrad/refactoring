help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

install: composer.lock ## Install composer
	composer install

check-all: code-quality-psalm-check code-quality-phpstan-check code-quality-rector-check code-style-check ## Dry run PhpStan Psalm Rector EasyCodingStandards

fix-all: code-quality-rector-fix code-style-fix ## Fix Rector EasyCodingStandards

code-style-check: ## Code style - Dry check
	vendor/bin/ecs check

code-style-fix: ## Code style - Fix code
	vendor/bin/ecs check --fix

code-quality-phpstan-check: ## Code quality - PHPStan - Check
	vendor/bin/phpstan analyse

code-quality-psalm-check: ## Code quality - Psalm - Check
	vendor/bin/psalm --long-progress --show-info=true

code-quality-rector-check: ## Code quality - Rector - Check
	vendor/bin/rector process module --dry-run

code-quality-rector-fix: ## Code quality - Rector - Fix
	vendor/bin/rector process module

tests: ## Run tests
	vendor/bin/phpunit -c phpunit.xml