<?php

declare(strict_types=1);

namespace TheatreConfiguration;

use Theatre\AmountCalculator;
use Theatre\AmountRules\BaseAmountForPerformance;
use Theatre\AmountRules\BonusAmountForAudienceAboveThanMinimumAudience;
use Theatre\AmountRules\BonusAmountForEachViewer;
use Theatre\AmountRules\BonusAmountForEachViewerAboveMinimumAudience;
use Theatre\Collection\AmountRules;
use Theatre\Collection\CreditVolumesRules;
use Theatre\CreditVolumesCalculator;
use Theatre\CreditVolumesRules\BonusCreditsForEachViewerAboveMinimumAudience;
use Theatre\CreditVolumesRules\BonusCreditVolumesForEachSpecifiedNumberOfViewers;
use Theatre\Service\AmountCalculatorService;
use Theatre\Service\CreditVolumesCalculatorService;
use Theatre\TheatreConfiguration;
use Theatre\ValueObject\Amount;
use Theatre\ValueObject\Audience;
use Theatre\ValueObject\CreditVolumes;
use Theatre\ValueObject\Play\Type;

final class WarsawTheatreConfiguration implements TheatreConfiguration
{
    public function amountCalculator(): AmountCalculator
    {
        $amountCalculatorService = new AmountCalculatorService();
        $amountCalculatorService->addAmountRules(
            Type::create(PlayType::COMEDY),
            new AmountRules(
                ...[
                    new BaseAmountForPerformance(Amount::create(30000)),
                    new BonusAmountForAudienceAboveThanMinimumAudience(Amount::create(10000), Audience::create(20)),
                    new BonusAmountForEachViewerAboveMinimumAudience(Amount::create(500), Audience::create(20)),
                    new BonusAmountForEachViewer(Amount::create(300)),
                ]
            )
        );
        $amountCalculatorService->addAmountRules(
            Type::create(PlayType::TRAGEDY),
            new AmountRules(
                ...[
                    new BaseAmountForPerformance(Amount::create(40000)),
                    new BonusAmountForEachViewerAboveMinimumAudience(Amount::create(1000), Audience::create(30)),
                ]
            )
        );

        return $amountCalculatorService;
    }

    public function creditVolumesCalculator(): CreditVolumesCalculator
    {
        $creditVolumesCalculatorService = new CreditVolumesCalculatorService();
        $creditVolumesCalculatorService->addCreditVolumesRules(
            Type::create(PlayType::COMEDY),
            new CreditVolumesRules(
                ...[
                    new BonusCreditsForEachViewerAboveMinimumAudience(CreditVolumes::create(1), Audience::create(30)),
                    new BonusCreditVolumesForEachSpecifiedNumberOfViewers(CreditVolumes::create(1), Audience::create(5)),
                ]
            )
        );
        $creditVolumesCalculatorService->addCreditVolumesRules(
            Type::create(PlayType::TRAGEDY),
            new CreditVolumesRules(
                ...[
                    new BonusCreditsForEachViewerAboveMinimumAudience(CreditVolumes::create(1), Audience::create(30)),
                ]
            )
        );

        return $creditVolumesCalculatorService;
    }

    public function invoicesRawDataFilePath(): string
    {
        return __DIR__ . '/../../../application/theatre/json/invoices.json';
    }

    public function playsRawDataFilePath(): string
    {
        return __DIR__ . '/../../../application/theatre/json/plays.json';
    }
}
