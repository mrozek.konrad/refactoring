<?php

declare(strict_types=1);

namespace TheatreConfiguration;

interface PlayType
{
    public const COMEDY  = 'comedy';
    public const TRAGEDY = 'tragedy';
}
