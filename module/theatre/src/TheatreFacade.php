<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\Invoices;

interface TheatreFacade
{
    public function prepareInvoicesTextSummary(Invoices $invoices): string;

    public function prepareInvoices(TheatreConfiguration $theatreConfiguration): Invoices;
}
