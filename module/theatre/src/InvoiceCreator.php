<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\Performances;
use Theatre\ValueObject\Customer;
use Theatre\ValueObject\Invoice;

interface InvoiceCreator
{
    public function create(Customer $customer, Performances $performances): Invoice;
}
