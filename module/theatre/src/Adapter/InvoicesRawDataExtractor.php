<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use RuntimeException;
use Theatre\FileReader;

final class InvoicesRawDataExtractor implements \Theatre\InvoicesRawDataExtractor
{
    public function __construct(
        private FileReader $fileReader
    ) {
    }

    public function extract(string $invoicesFilePath, int $invoiceNumber): array
    {
        $invoicesRawData = $this->fileReader->read($invoicesFilePath);

        return $this->prepareInvoiceRawData($invoicesRawData, $invoiceNumber);
    }

    private function prepareInvoiceRawData(array $invoicesRawData, int $invoiceNumber): array
    {
        if (! isset($invoicesRawData[$invoiceNumber])) {
            throw new RuntimeException(\sprintf('Invoice %d not found in selected invoices file.', $invoiceNumber));
        }

        if (! \is_array($invoicesRawData[$invoiceNumber])) {
            throw new RuntimeException(\sprintf('Invoice %d has invalid data.', $invoiceNumber));
        }

        return $invoicesRawData[$invoiceNumber];
    }
}
