<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use Theatre\Collection\Invoices;
use Theatre\Collection\Plays;
use Theatre\CustomerParser;
use Theatre\InvoiceCreator;
use Theatre\InvoicePrinter;
use Theatre\InvoicesCounter;
use Theatre\PerformancesParser;
use Theatre\PlaysParser;
use Theatre\Service\InvoiceCreatorService;
use Theatre\Service\PerformancesSummaryCreatorService;
use Theatre\TheatreConfiguration;
use Theatre\ValueObject\Invoice;

final class TheatreFacade implements \Theatre\TheatreFacade
{
    public function __construct(
        private CustomerParser $customerParser,
        private PerformancesParser $performancesParser,
        private PlaysParser $playsParser,
        private InvoicesCounter $invoicesCounter,
        private InvoicePrinter $invoicePrinter
    ) {
    }

    public function prepareInvoices(TheatreConfiguration $theatreConfiguration): Invoices
    {
        $invoiceCreator = $this->prepareInvoiceCreator($theatreConfiguration);
        $plays          = $this->playsParser->parse($theatreConfiguration->playsRawDataFilePath());

        return $this->createInvoices($theatreConfiguration, $invoiceCreator, $plays);
    }

    public function prepareInvoicesTextSummary(Invoices $invoices): string
    {
        $summary = '';

        foreach ($invoices as $invoice) {
            $summary .= $this->invoicePrinter->print($invoice) . PHP_EOL;
        }

        return $summary;
    }

    private function prepareInvoiceCreator(TheatreConfiguration $theatreConfiguration): InvoiceCreator
    {
        $performancesSummaryCreatorService = new PerformancesSummaryCreatorService(
            $theatreConfiguration->amountCalculator(),
            $theatreConfiguration->creditVolumesCalculator(),
        );

        return new InvoiceCreatorService($performancesSummaryCreatorService);
    }

    private function createInvoices(TheatreConfiguration $theatreConfiguration, InvoiceCreator $invoiceCreator, Plays $plays): Invoices
    {
        $invoicesToCreate = $this->invoicesCounter->count($theatreConfiguration->invoicesRawDataFilePath());

        $invoices = [];

        for ($invoiceNumber = 0; $invoiceNumber < $invoicesToCreate; ++$invoiceNumber) {
            $invoices[] = $this->prepareInvoice($invoiceCreator, $theatreConfiguration, $plays, $invoiceNumber);
        }

        return new Invoices(...$invoices);
    }

    private function prepareInvoice(
        InvoiceCreator $invoiceCreator,
        TheatreConfiguration $theatreConfiguration,
        Plays $plays,
        int $invoiceNumber
    ): Invoice {
        $customer     = $this->customerParser->parse($theatreConfiguration->invoicesRawDataFilePath(), $invoiceNumber);
        $performances = $this->performancesParser->parse($plays, $theatreConfiguration->invoicesRawDataFilePath(), $invoiceNumber);

        return $invoiceCreator->create($customer, $performances);
    }
}
