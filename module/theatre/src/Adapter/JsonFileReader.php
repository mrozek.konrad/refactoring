<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use RuntimeException;
use Theatre\FileReader;

final class JsonFileReader implements FileReader
{
    public function read(string $filePath): array
    {
        $this->assertFileExists($filePath);

        return $this->decode($filePath);
    }

    private function assertFileExists(string $filePath): void
    {
        if (! \file_exists($filePath)) {
            throw new RuntimeException(\sprintf('File does not exist in path: %s', $filePath));
        }
    }

    private function decode(string $filePath): array
    {
        $decodeResult = \json_decode(\file_get_contents($filePath), true);

        if (false === $decodeResult) {
            throw new RuntimeException(\sprintf('Invalid JSON file: %s', \json_last_error_msg()));
        }

        return $decodeResult;
    }
}
