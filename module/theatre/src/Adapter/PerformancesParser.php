<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use RuntimeException;
use Theatre\Collection\Performances;
use Theatre\Collection\Plays;
use Theatre\ValueObject\Audience;
use Theatre\ValueObject\Performance;
use Theatre\ValueObject\Play\Id;

final class PerformancesParser implements \Theatre\PerformancesParser
{
    private const FIELD_PERFORMANCES         = 'performances';
    private const FIELD_PERFORMANCE_PLAY_ID  = 'playId';
    private const FIELD_PERFORMANCE_AUDIENCE = 'audience';

    public function __construct(
        private \Theatre\InvoicesRawDataExtractor $invoicesRawDataExtractor
    ) {
    }

    public function parse(Plays $plays, string $invoicesFilePath, int $invoiceNumber): Performances
    {
        $rawPerformanceData = $this->invoicesRawDataExtractor->extract($invoicesFilePath, $invoiceNumber);

        $this->validateRawPerformancesStructure($rawPerformanceData);

        return $this->extractPerformances($plays, $rawPerformanceData);
    }

    private function validateRawPerformancesStructure(array $rawPerformancesData): void
    {
        if (! isset($rawPerformancesData[self::FIELD_PERFORMANCES])) {
            throw new RuntimeException(\sprintf('Missing %s field in performances data.', self::FIELD_PERFORMANCES));
        }

        foreach ($rawPerformancesData[self::FIELD_PERFORMANCES] as $performance) {
            if (! isset($performance[self::FIELD_PERFORMANCE_PLAY_ID])) {
                throw new RuntimeException(\sprintf('Missing %s field in performance data.', self::FIELD_PERFORMANCE_PLAY_ID));
            }

            if (! isset($performance[self::FIELD_PERFORMANCE_AUDIENCE])) {
                throw new RuntimeException(\sprintf('Missing %s field in performance data.', self::FIELD_PERFORMANCE_AUDIENCE));
            }
        }
    }

    private function extractPerformances(Plays $plays, array $rawPerformancesData): Performances
    {
        $rawPerformances = $rawPerformancesData[self::FIELD_PERFORMANCES];

        $performances = [];

        foreach ($rawPerformances as $rawPerformance) {
            $playId = Id::create($rawPerformance[self::FIELD_PERFORMANCE_PLAY_ID]);

            $audience = Audience::create($rawPerformance[self::FIELD_PERFORMANCE_AUDIENCE]);
            $play     = $plays->find($playId);

            $performances[] = new Performance($play, $audience);
        }

        return new Performances(...$performances);
    }
}
