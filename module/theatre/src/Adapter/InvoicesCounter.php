<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use Theatre\FileReader;

final class InvoicesCounter implements \Theatre\InvoicesCounter
{
    public function __construct(
        private FileReader $fileReader
    ) {
    }

    public function count(string $invoicesFilePath): int
    {
        $invoices = $this->fileReader->read($invoicesFilePath);

        return \count($invoices);
    }
}
