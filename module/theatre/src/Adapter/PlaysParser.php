<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use RuntimeException;
use Theatre\Collection\Plays;
use Theatre\FileReader;
use Theatre\ValueObject\Play;

final class PlaysParser implements \Theatre\PlaysParser
{
    private const FIELD_PERFORMANCE_NAME = 'name';
    private const FIELD_PERFORMANCE_TYPE = 'type';

    public function __construct(
        private FileReader $fileReader
    ) {
    }

    public function parse(string $playsFilePath): Plays
    {
        $rawPlaysData = $this->fileReader->read($playsFilePath);

        $this->validateRawPlaysStructure($rawPlaysData);

        return $this->extractPlays($rawPlaysData);
    }

    private function validateRawPlaysStructure(array $rawPlaysData): void
    {
        foreach ($rawPlaysData as $rawPlay) {
            if (! isset($rawPlay[self::FIELD_PERFORMANCE_NAME])) {
                throw new RuntimeException(\sprintf('Missing %s field in plays data.', self::FIELD_PERFORMANCE_TYPE));
            }

            if (! isset($rawPlay[self::FIELD_PERFORMANCE_TYPE])) {
                throw new RuntimeException(\sprintf('Missing %s field in plays data.', self::FIELD_PERFORMANCE_TYPE));
            }
        }
    }

    private function extractPlays(array $rawPlaysData): Plays
    {
        $plays = [];

        foreach ($rawPlaysData as $id => $rawPlay) {
            $plays[] = new Play(
                Play\Id::create($id),
                Play\Name::create($rawPlay['name']),
                Play\Type::create($rawPlay['type']),
            );
        }

        return new Plays(...$plays);
    }
}
