<?php

declare(strict_types=1);

namespace Theatre\Adapter;

use RuntimeException;
use Theatre\ValueObject\Customer;

final class CustomerParser implements \Theatre\CustomerParser
{
    private const FIELD_CUSTOMER = 'customer';

    public function __construct(
        private \Theatre\InvoicesRawDataExtractor $invoicesRawDataExtractor
    ) {
    }

    public function parse(string $invoicesFilePath, int $invoiceNumber): Customer
    {
        $invoiceRawData = $this->invoicesRawDataExtractor->extract($invoicesFilePath, $invoiceNumber);

        $this->validateRawCustomerStructure($invoiceRawData);

        return $this->extractCustomer($invoiceRawData);
    }

    private function validateRawCustomerStructure(array $rawPlaysData): void
    {
        if (! isset($rawPlaysData[self::FIELD_CUSTOMER])) {
            throw new RuntimeException(\sprintf('Missing %s field in customer data.', self::FIELD_CUSTOMER));
        }
    }

    private function extractCustomer(array $rawCustomerData): Customer
    {
        return new Customer($rawCustomerData[self::FIELD_CUSTOMER]);
    }
}
