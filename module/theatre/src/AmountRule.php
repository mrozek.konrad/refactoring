<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\ValueObject\Amount;
use Theatre\ValueObject\Audience;

interface AmountRule
{
    public function calculateAmount(Audience $audience): Amount;
}
