<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\ValueObject\Audience;
use Theatre\ValueObject\CreditVolumes;

interface CreditVolumesRule
{
    public function calculateCredit(Audience $audience): CreditVolumes;
}
