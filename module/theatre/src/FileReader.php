<?php

declare(strict_types=1);

namespace Theatre;

interface FileReader
{
    public function read(string $filePath): array;
}
