<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\Plays;

interface PlaysParser
{
    public function parse(string $playsFilePath): Plays;
}
