<?php

declare(strict_types=1);

namespace Theatre\Collection;

use ArrayIterator;
use Theatre\ValueObject\Performance;

final class Performances extends ArrayIterator
{
    public function __construct(Performance ...$performances)
    {
        parent::__construct($performances);
    }
}
