<?php

declare(strict_types=1);

namespace Theatre\Collection;

use ArrayIterator;
use Theatre\AmountRule;

final class AmountRules extends ArrayIterator
{
    public function __construct(AmountRule ...$amountRules)
    {
        parent::__construct($amountRules);
    }
}
