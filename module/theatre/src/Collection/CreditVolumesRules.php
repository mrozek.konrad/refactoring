<?php

declare(strict_types=1);

namespace Theatre\Collection;

use ArrayIterator;
use Theatre\CreditVolumesRule;

final class CreditVolumesRules extends ArrayIterator
{
    public function __construct(CreditVolumesRule ...$creditVolumesRules)
    {
        parent::__construct($creditVolumesRules);
    }
}
