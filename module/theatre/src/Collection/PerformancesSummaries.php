<?php

declare(strict_types=1);

namespace Theatre\Collection;

use ArrayIterator;
use Theatre\ValueObject\PerformanceSummary;

final class PerformancesSummaries extends ArrayIterator
{
    public function __construct(PerformanceSummary ...$performancesSummaries)
    {
        parent::__construct($performancesSummaries);
    }
}
