<?php

declare(strict_types=1);

namespace Theatre\Collection;

use ArrayIterator;
use Theatre\ValueObject\Invoice;

final class Invoices extends ArrayIterator
{
    public function __construct(Invoice ...$invoices)
    {
        parent::__construct($invoices);
    }
}
