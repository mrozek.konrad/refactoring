<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\ValueObject\Invoice;

interface InvoicePrinter
{
    public function print(Invoice $invoice): string;
}
