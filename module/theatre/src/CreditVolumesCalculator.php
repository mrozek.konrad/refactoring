<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\CreditVolumesRules;
use Theatre\Collection\PerformancesSummaries;
use Theatre\ValueObject\CreditVolumes;
use Theatre\ValueObject\Performance;

interface CreditVolumesCalculator
{
    public function addCreditVolumesRules(ValueObject\Play\Type $playType, CreditVolumesRules $creditVolumesRules): void;

    public function calculate(Performance $performance): CreditVolumes;

    public function calculateTotalCreditVolumes(PerformancesSummaries $performancesSummaries): CreditVolumes;

    public function creditVolumes(ValueObject\Play\Type $playType): CreditVolumesRules;
}
