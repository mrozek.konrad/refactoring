<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Adapter\CustomerParser;
use Theatre\Adapter\InvoicesCounter;
use Theatre\Adapter\InvoicesRawDataExtractor;
use Theatre\Adapter\JsonFileReader;
use Theatre\Adapter\PerformancesParser;
use Theatre\Adapter\PlaysParser;
use Theatre\Service\InvoicePrinterService;

final class TheatreFacadeFactory
{
    public function create(): TheatreFacade
    {
        $jsonFileReader = new JsonFileReader();

        $invoicesRawDataExtractor = new InvoicesRawDataExtractor($jsonFileReader);

        $customerParser     = new CustomerParser($invoicesRawDataExtractor);
        $performancesParser = new PerformancesParser($invoicesRawDataExtractor);
        $playsParser        = new PlaysParser($jsonFileReader);

        $invoicePrinterService = new InvoicePrinterService();
        $invoicesCounter       = new InvoicesCounter($jsonFileReader);

        return new \Theatre\Adapter\TheatreFacade($customerParser, $performancesParser, $playsParser, $invoicesCounter, $invoicePrinterService);
    }
}
