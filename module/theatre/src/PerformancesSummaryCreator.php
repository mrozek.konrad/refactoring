<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\Performances;
use Theatre\ValueObject\PerformancesSummary;

interface PerformancesSummaryCreator
{
    public function createSummary(Performances $performances): PerformancesSummary;
}
