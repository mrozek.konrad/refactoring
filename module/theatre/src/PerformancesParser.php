<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\Performances;
use Theatre\Collection\Plays;

interface PerformancesParser
{
    public function parse(Plays $plays, string $invoicesFilePath, int $invoiceNumber): Performances;
}
