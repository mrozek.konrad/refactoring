<?php

declare(strict_types=1);

namespace Theatre\Service;

use RuntimeException;
use Theatre\Collection\CreditVolumesRules;
use Theatre\Collection\PerformancesSummaries;
use Theatre\CreditVolumesCalculator;
use Theatre\ValueObject\CreditVolumes;
use Theatre\ValueObject\Performance;
use Theatre\ValueObject\Play\Type;

final class CreditVolumesCalculatorService implements CreditVolumesCalculator
{
    private array $creditVolumesRules = [];

    public function addCreditVolumesRules(Type $playType, CreditVolumesRules $creditVolumesRules): void
    {
        $this->creditVolumesRules[$playType->value()] = $creditVolumesRules;
    }

    public function calculate(Performance $performance): CreditVolumes
    {
        $creditVolumesRules = $this->creditVolumes($performance->play()->type());

        $creditVolumes = CreditVolumes::zero();

        foreach ($creditVolumesRules as $creditVolumesRule) {
            $calculatedCreditVolumes = $creditVolumesRule->calculateCredit($performance->audience());

            $creditVolumes = $creditVolumes->add($calculatedCreditVolumes);
        }

        return $creditVolumes;
    }

    public function calculateTotalCreditVolumes(PerformancesSummaries $performancesSummaries): CreditVolumes
    {
        $totalVolumeCredits = CreditVolumes::zero();

        foreach ($performancesSummaries as $performancesSummary) {
            $totalVolumeCredits = $totalVolumeCredits->add($performancesSummary->creditVolumes());
        }

        return $totalVolumeCredits;
    }

    public function creditVolumes(Type $playType): CreditVolumesRules
    {
        if (! \array_key_exists($playType->value(), $this->creditVolumesRules)) {
            throw new RuntimeException('Credit volumes rules for play type ' . $playType->value() . ' are not set.');
        }

        return $this->creditVolumesRules[$playType->value()];
    }
}
