<?php

declare(strict_types=1);

namespace Theatre\Service;

use Theatre\InvoicePrinter;
use Theatre\ValueObject\Amount;
use Theatre\ValueObject\Customer;
use Theatre\ValueObject\Invoice;
use Theatre\ValueObject\Performance;
use Theatre\ValueObject\PerformancesSummary;
use Theatre\ValueObject\PerformanceSummary;

final class InvoicePrinterService implements InvoicePrinter
{
    public function print(Invoice $invoice): string
    {
        $result = $this->printCustomerInfo($invoice->customer());

        return $result . $this->printPerformancesSummary($invoice->performancesSummary());
    }

    private function printCustomerInfo(Customer $customer): string
    {
        return 'Rachunek dla ' . $customer->name() . PHP_EOL;
    }

    private function printPerformancesSummary(PerformancesSummary $performancesSummary): string
    {
        $result = '';

        foreach ($performancesSummary->performancesSummaries() as $performanceSummary) {
            $result .= $this->printPerformanceSummary($performanceSummary);
        }

        $result .= $this->formatTotalAmount($performancesSummary);
        $result .= $this->formatTotalCreditVolumes($performancesSummary);

        return $result;
    }

    private function printPerformanceSummary(PerformanceSummary $performanceSummary): string
    {
        $result = $this->formatPlay($performanceSummary->performance());
        $result .= $this->formatAmount($performanceSummary->amount());
        $result .= $this->formatAudience($performanceSummary->performance()) . PHP_EOL;

        return $result;
    }

    private function formatTotalAmount(PerformancesSummary $performancesSummary): string
    {
        return 'Naleznosc: ' . \number_format($performancesSummary->totalAmount()->value() / 100) . PHP_EOL;
    }

    private function formatTotalCreditVolumes(PerformancesSummary $performancesSummary): string
    {
        return 'Punkty promocyjne: ' . (string) $performancesSummary->totalCreditVolumes()->value() . PHP_EOL;
    }

    private function formatPlay(Performance $performance): string
    {
        return ' ' . (string) $performance->play()->name() . ': ';
    }

    private function formatAmount(Amount $amount): string
    {
        return \number_format($amount->value() / 100, 2);
    }

    private function formatAudience(Performance $performance): string
    {
        return ' (liczba miejsc: ' . (string) $performance->audience()->value() . ')';
    }
}
