<?php

declare(strict_types=1);

namespace Theatre\Service;

use Theatre\AmountCalculator;
use Theatre\Collection\Performances;
use Theatre\Collection\PerformancesSummaries;
use Theatre\CreditVolumesCalculator;
use Theatre\PerformancesSummaryCreator;
use Theatre\ValueObject\Performance;
use Theatre\ValueObject\PerformancesSummary;
use Theatre\ValueObject\PerformanceSummary;

final class PerformancesSummaryCreatorService implements PerformancesSummaryCreator
{
    public function __construct(
        private AmountCalculator $amountCalculator,
        private CreditVolumesCalculator $creditVolumesCalculator
    ) {
    }

    public function createSummary(Performances $performances): PerformancesSummary
    {
        $performancesSummaries = $this->createPerformancesSummaries($performances);
        $totalAmount           = $this->amountCalculator->calculateTotalAmount($performancesSummaries);
        $totalCreditVolumes    = $this->creditVolumesCalculator->calculateTotalCreditVolumes($performancesSummaries);

        return new PerformancesSummary($performancesSummaries, $totalAmount, $totalCreditVolumes);
    }

    private function createPerformancesSummaries(Performances $performances): PerformancesSummaries
    {
        $performancesSummaries = [];

        foreach ($performances as $performance) {
            $performancesSummaries[] = $this->createPerformanceSummary($performance);
        }

        return new PerformancesSummaries(...$performancesSummaries);
    }

    private function createPerformanceSummary(Performance $performance): PerformanceSummary
    {
        $performanceCalculatedAmount        = $this->amountCalculator->calculate($performance);
        $performanceCalculatedCreditVolumes = $this->creditVolumesCalculator->calculate($performance);

        return new PerformanceSummary($performance, $performanceCalculatedAmount, $performanceCalculatedCreditVolumes);
    }
}
