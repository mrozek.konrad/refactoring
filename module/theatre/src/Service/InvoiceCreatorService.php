<?php

declare(strict_types=1);

namespace Theatre\Service;

use Theatre\Collection\Performances;
use Theatre\InvoiceCreator;
use Theatre\PerformancesSummaryCreator;
use Theatre\ValueObject\Customer;
use Theatre\ValueObject\Invoice;

final class InvoiceCreatorService implements InvoiceCreator
{
    public function __construct(
        private PerformancesSummaryCreator $performancesSummaryCreator
    ) {
    }

    public function create(Customer $customer, Performances $performances): Invoice
    {
        $performancesSummary = $this->performancesSummaryCreator->createSummary($performances);

        return new Invoice($customer, $performancesSummary);
    }
}
