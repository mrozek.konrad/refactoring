<?php

declare(strict_types=1);

namespace Theatre\CreditVolumesRules;

use Theatre\CreditVolumesRule;
use Theatre\ValueObject\Audience;
use Theatre\ValueObject\CreditVolumes;

final class BonusCreditsForEachViewerAboveMinimumAudience implements CreditVolumesRule
{
    public function __construct(
        private CreditVolumes $creditVolumes,
        private Audience $audience
    ) {
    }

    public function calculateCredit(Audience $audience): CreditVolumes
    {
        $viewersAboveMinimumAudience = $audience->minus($this->audience);

        return $this->creditVolumes->multiply($viewersAboveMinimumAudience->value());
    }
}
