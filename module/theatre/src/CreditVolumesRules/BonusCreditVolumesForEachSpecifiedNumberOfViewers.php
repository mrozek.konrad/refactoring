<?php

declare(strict_types=1);

namespace Theatre\CreditVolumesRules;

use Theatre\CreditVolumesRule;
use Theatre\ValueObject\Audience;
use Theatre\ValueObject\CreditVolumes;

final class BonusCreditVolumesForEachSpecifiedNumberOfViewers implements CreditVolumesRule
{
    public function __construct(
        private CreditVolumes $creditVolumes,
        private Audience $audience
    ) {
    }

    public function calculateCredit(Audience $audience): CreditVolumes
    {
        $numberOfPrizes = \floor($audience->value() / $this->audience->value());

        return $this->creditVolumes->multiply((int) $numberOfPrizes);
    }
}
