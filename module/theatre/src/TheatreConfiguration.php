<?php

declare(strict_types=1);

namespace Theatre;

interface TheatreConfiguration
{
    public function amountCalculator(): AmountCalculator;

    public function creditVolumesCalculator(): CreditVolumesCalculator;

    public function invoicesRawDataFilePath(): string;

    public function playsRawDataFilePath(): string;
}
