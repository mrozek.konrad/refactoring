<?php

declare(strict_types=1);

namespace Theatre;

interface InvoicesCounter
{
    public function count(string $invoicesFilePath): int;
}
