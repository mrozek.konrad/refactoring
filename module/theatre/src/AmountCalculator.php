<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\Collection\AmountRules;
use Theatre\Collection\PerformancesSummaries;
use Theatre\ValueObject\Amount;
use Theatre\ValueObject\Performance;
use Theatre\ValueObject\Play\Type;

interface AmountCalculator
{
    public function addAmountRules(Type $playType, AmountRules $amountRules): void;

    public function calculate(Performance $performance): Amount;

    public function calculateTotalAmount(PerformancesSummaries $performancesSummaries): Amount;

    public function amountRules(Type $playType): AmountRules;
}
