<?php

declare(strict_types=1);

namespace Theatre\AmountRules;

use Theatre\AmountRule;
use Theatre\ValueObject\Amount;
use Theatre\ValueObject\Audience;

final class BonusAmountForEachViewerAboveMinimumAudience implements AmountRule
{
    public function __construct(
        private Amount $amount,
        private Audience $audience
    ) {
    }

    public function calculateAmount(Audience $audience): Amount
    {
        $viewersAboveMinimumAudience = $audience->minus($this->audience);

        return $this->amount->multiply($viewersAboveMinimumAudience->value());
    }
}
