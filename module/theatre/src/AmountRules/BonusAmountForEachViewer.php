<?php

declare(strict_types=1);

namespace Theatre\AmountRules;

use Theatre\AmountRule;
use Theatre\ValueObject\Amount;
use Theatre\ValueObject\Audience;

final class BonusAmountForEachViewer implements AmountRule
{
    public function __construct(
        private Amount $amount
    ) {
    }

    public function calculateAmount(Audience $audience): Amount
    {
        return $this->amount->multiply($audience->value());
    }
}
