<?php

declare(strict_types=1);

namespace Theatre;

use Theatre\ValueObject\Customer;

interface CustomerParser
{
    public function parse(string $invoicesFilePath, int $invoiceNumber): Customer;
}
