<?php

declare(strict_types=1);

namespace Theatre;

interface InvoicesRawDataExtractor
{
    public function extract(string $invoicesFilePath, int $invoiceNumber): array;
}
