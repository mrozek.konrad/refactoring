<?php

declare(strict_types=1);

namespace Theatre\ValueObject;

use Theatre\ValueObject\Play\Id;
use Theatre\ValueObject\Play\Name;
use Theatre\ValueObject\Play\Type;

final class Play
{
    public function __construct(
        private Id $id,
        private Name $name,
        private Type $type
    ) {
    }

    public function id(): Id
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function type(): Type
    {
        return $this->type;
    }

    public static function create(Id $id, Name $name, Type $type): self
    {
        return new self($id, $name, $type);
    }
}
