<?php

declare(strict_types=1);

namespace Theatre\ValueObject;

final class Performance
{
    public function __construct(
        private Play $play,
        private Audience $audience
    ) {
    }

    public function audience(): Audience
    {
        return $this->audience;
    }

    public function play(): Play
    {
        return $this->play;
    }
}
