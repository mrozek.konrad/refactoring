<?php

declare(strict_types=1);

namespace Theatre\ValueObject;

final class PerformanceSummary
{
    public function __construct(
        private Performance $performance,
        private Amount $amount,
        private CreditVolumes $creditVolumes
    ) {
    }

    public function amount(): Amount
    {
        return $this->amount;
    }

    public function creditVolumes(): CreditVolumes
    {
        return $this->creditVolumes;
    }

    public function performance(): Performance
    {
        return $this->performance;
    }
}
