<?php

declare(strict_types=1);

namespace Theatre\ValueObject\Play;

use InvalidArgumentException;

final class Type
{
    public const LENGTH_MINIMUM = 5;
    public const LENGTH_MAXIMUM = 25;

    private string $type;

    public function __construct(string $type)
    {
        $lengthPlayId = \strlen($type);

        if ($lengthPlayId < self::LENGTH_MINIMUM || $lengthPlayId > self::LENGTH_MAXIMUM) {
            throw new InvalidArgumentException(\sprintf('Length of type must be between %d-%d chars.', self::LENGTH_MINIMUM, self::LENGTH_MAXIMUM));
        }

        $this->type = $type;
    }

    public function value(): string
    {
        return $this->type;
    }

    public static function create(string $type): self
    {
        return new self($type);
    }
}
