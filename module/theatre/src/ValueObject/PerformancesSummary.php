<?php

declare(strict_types=1);

namespace Theatre\ValueObject;

use Theatre\Collection\PerformancesSummaries;

final class PerformancesSummary
{
    public function __construct(
        private PerformancesSummaries $performancesSummaries,
        private Amount $amount,
        private CreditVolumes $creditVolumes
    ) {
    }

    public function performancesSummaries(): PerformancesSummaries
    {
        return $this->performancesSummaries;
    }

    public function totalAmount(): Amount
    {
        return $this->amount;
    }

    public function totalCreditVolumes(): CreditVolumes
    {
        return $this->creditVolumes;
    }
}
