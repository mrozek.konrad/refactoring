<?php

declare(strict_types=1);

namespace Theatre\ValueObject;

final class Invoice
{
    public function __construct(
        private Customer $customer,
        private PerformancesSummary $performancesSummary
    ) {
    }

    public function customer(): Customer
    {
        return $this->customer;
    }

    public function performancesSummary(): PerformancesSummary
    {
        return $this->performancesSummary;
    }
}
