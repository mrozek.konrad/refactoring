<?php

declare(strict_types=1);

namespace Theatre\Tests\AmountRules;

use Theatre\AmountRule;
use Theatre\Tests\Fixtures\AmountRulesFixtures;
use Theatre\Tests\TheatreTestCase;

final class BonusAmountForEachViewerTest extends TheatreTestCase
{
    use AmountRulesFixtures;

    public function testCalculatesCorrectResult(): void
    {
        $amount                   = $this->amount();
        $audience                 = $this->audience();

        $expectedBonusAmount = $amount->multiply($audience->value());

        $rule             = $this->buildBonusAmountForEachViewerRule($amount);
        $calculatedAmount = $rule->calculateAmount($audience);

        $this->assertTrue($expectedBonusAmount->areEquals($calculatedAmount));
    }

    public function testIsTypeOfAmountRule(): void
    {
        $rule = $this->buildBonusAmountForEachViewerRule();

        $this->assertInstanceOf(AmountRule::class, $rule);
    }
}
