<?php

declare(strict_types=1);

namespace Theatre\Tests\ValueObject;

use Theatre\Tests\Fixtures\PerformanceFixtures;
use Theatre\Tests\TheatreTestCase;
use Theatre\ValueObject\PerformanceSummary;

final class PerformanceSummaryTest extends TheatreTestCase
{
    use PerformanceFixtures;

    public function testContainsCorrectObjectsAfterCreation(): void
    {
        $performance   = $this->performance();
        $amount        = $this->amount();
        $creditVolumes = $this->creditVolumes();

        $performanceSummary = new PerformanceSummary($performance, $amount, $creditVolumes);

        $this->assertSame($performance, $performanceSummary->performance());
        $this->assertSame($amount, $performanceSummary->amount());
        $this->assertSame($creditVolumes, $performanceSummary->creditVolumes());
    }
}
