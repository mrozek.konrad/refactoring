<?php

declare(strict_types=1);

namespace Theatre\Tests\ValueObject;

use Theatre\Tests\Fixtures\PerformanceFixtures;
use Theatre\Tests\TheatreTestCase;
use Theatre\ValueObject\PerformancesSummary;

final class PerformancesSummaryTest extends TheatreTestCase
{
    use PerformanceFixtures;

    public function testCreatedObjectHasValidState(): void
    {
        $performancesSummaries = $this->performancesSummaries();
        $totalAmount           = $this->amount();
        $totalCreditVolumes    = $this->creditVolumes();

        $performancesSummary = new PerformancesSummary($performancesSummaries, $totalAmount, $totalCreditVolumes);

        $this->assertSame($performancesSummaries, $performancesSummary->performancesSummaries());
        $this->assertSame($totalAmount, $performancesSummary->totalAmount());
        $this->assertSame($totalCreditVolumes, $performancesSummary->totalCreditVolumes());
    }
}
