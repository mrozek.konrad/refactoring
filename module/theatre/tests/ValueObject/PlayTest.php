<?php

declare(strict_types=1);

namespace Theatre\Tests\ValueObject;

use Theatre\Tests\Fixtures\PlayFixtures;
use Theatre\Tests\TheatreTestCase;
use Theatre\ValueObject\Play;

final class PlayTest extends TheatreTestCase
{
    use PlayFixtures;

    public function testPlayReturnsValidIdAndNameAndType(): void
    {
        $id   = $this->playId();
        $name = $this->playName();
        $type = $this->playType();

        $play = Play::create($id, $name, $type);

        $this->assertSame($id, $play->id());
        $this->assertSame($name, $play->name());
        $this->assertSame($type, $play->type());
    }
}
