<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\ValueObject\CreditVolumes;

trait CreditVolumesFixtures
{
    use RandomScalarValuesFixtures;

    final protected function creditVolumes(?int $value = null): CreditVolumes
    {
        return CreditVolumes::create($value ?? $this->creditVolumesValue());
    }

    final protected function creditVolumesValue(): int
    {
        return $this->mediumValue();
    }

    final protected function creditVolumesValueGreaterThan(int $credit): int
    {
        return $this->valueGreaterThan($credit);
    }

    final protected function creditVolumesValueLessThanZero(): int
    {
        return $this->valueLowerThan($this->zero());
    }
}
