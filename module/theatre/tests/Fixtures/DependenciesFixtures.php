<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\AmountCalculator;
use Theatre\CreditVolumesCalculator;
use Theatre\InvoiceCreator;
use Theatre\InvoicePrinter;
use Theatre\PerformancesSummaryCreator;
use Theatre\Service\AmountCalculatorService;
use Theatre\Service\CreditVolumesCalculatorService;
use Theatre\Service\InvoiceCreatorService;
use Theatre\Service\InvoicePrinterService;
use Theatre\Service\PerformancesSummaryCreatorService;

trait DependenciesFixtures
{
    final public function amountCalculator(): AmountCalculator
    {
        return new AmountCalculatorService();
    }

    final public function creditVolumesCalculator(): CreditVolumesCalculator
    {
        return new CreditVolumesCalculatorService();
    }

    final public function invoiceCreator(
        ?PerformancesSummaryCreator $performancesSummaryCreator = null
    ): InvoiceCreator {
        return new InvoiceCreatorService(
            $performancesSummaryCreator ?? $this->performancesSummaryCreator()
        );
    }

    final public function invoicePrinter(): InvoicePrinter
    {
        return new InvoicePrinterService();
    }

    final public function performancesSummaryCreator(
        ?AmountCalculator $amountCalculator = null,
        ?CreditVolumesCalculator $creditVolumesCalculator = null,
    ): PerformancesSummaryCreator {
        return new PerformancesSummaryCreatorService(
            $amountCalculator ?? $this->amountCalculator(),
            $creditVolumesCalculator ?? $this->creditVolumesCalculator(),
        );
    }
}
