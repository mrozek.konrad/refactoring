<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\Collection\Plays;
use Theatre\ValueObject\Play;
use Theatre\ValueObject\Play\Id;
use Theatre\ValueObject\Play\Type;

trait PlayFixtures
{
    use RandomScalarValuesFixtures;

    final protected function invalidPlayParamsWithFewPlaysWithTheSameId(\Theatre\ValueObject\Play\Id $id): array
    {
        return \array_merge($this->validPlaysParams($id), $this->validPlaysParams($id));
    }

    final protected function invalidPlaysParams(): array
    {
        return $this->arrayOf(fn () => $this->largeValue());
    }

    final protected function play(?Id $id = null): Play
    {
        return new Play($id ?? $this->playId(), $this->playName(), $this->playType());
    }

    final protected function playId(): \Theatre\ValueObject\Play\Id
    {
        return \Theatre\ValueObject\Play\Id::create($this->playIdValue());
    }

    final protected function playIdValue(): string
    {
        return $this->text($this->value(\Theatre\ValueObject\Play\Id::LENGTH_MINIMUM, \Theatre\ValueObject\Play\Id::LENGTH_MAXIMUM));
    }

    final protected function playIdValueTooLong(): string
    {
        return $this->textLongerThan(\Theatre\ValueObject\Play\Id::LENGTH_MAXIMUM);
    }

    final protected function playIdValueTooShort(): string
    {
        return $this->textShorterThan(\Theatre\ValueObject\Play\Id::LENGTH_MINIMUM);
    }

    final protected function playName(): \Theatre\ValueObject\Play\Name
    {
        return \Theatre\ValueObject\Play\Name::create($this->nameValue());
    }

    final protected function playType(): Type
    {
        return Type::create($this->typeValue());
    }

    final protected function plays(array $plays = []): Plays
    {
        $params = $plays ?: $this->validPlaysParams();

        return new Plays(...$params);
    }

    final protected function playsWithPlay(Play $play): Plays
    {
        $params   = $this->validPlaysParams();
        $params[] = $play;

        return new Plays(...$params);
    }

    final protected function nameValue(): string
    {
        return $this->text($this->value(\Theatre\ValueObject\Play\Name::LENGTH_MINIMUM, \Theatre\ValueObject\Play\Name::LENGTH_MAXIMUM));
    }

    final protected function nameValueTooLong(): string
    {
        return $this->textLongerThan(\Theatre\ValueObject\Play\Name::LENGTH_MAXIMUM);
    }

    final protected function nameValueTooShort(): string
    {
        return $this->textShorterThan(\Theatre\ValueObject\Play\Name::LENGTH_MINIMUM);
    }

    final protected function typeValue(): string
    {
        return $this->text($this->value(\Theatre\ValueObject\Play\Type::LENGTH_MINIMUM, \Theatre\ValueObject\Play\Type::LENGTH_MAXIMUM));
    }

    final protected function typeValueTooLong(): string
    {
        return $this->textLongerThan(\Theatre\ValueObject\Play\Type::LENGTH_MAXIMUM);
    }

    final protected function typeValueTooShort(): string
    {
        return $this->textShorterThan(\Theatre\ValueObject\Play\Type::LENGTH_MINIMUM);
    }

    final protected function validPlaysParams(?\Theatre\ValueObject\Play\Id $id = null): array
    {
        return $this->arrayOf(fn () => $this->play($id));
    }
}
