<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\Collection\Performances;
use Theatre\Collection\PerformancesSummaries;
use Theatre\ValueObject\Performance;
use Theatre\ValueObject\PerformancesSummary;
use Theatre\ValueObject\PerformanceSummary;

trait PerformanceFixtures
{
    use PlayFixtures;
    use AudienceFixtures;
    use AmountFixtures;
    use CreditVolumesFixtures;

    protected function performance(): Performance
    {
        return new Performance($this->play(), $this->audience());
    }

    protected function performanceSummary(): PerformanceSummary
    {
        return new PerformanceSummary($this->performance(), $this->amount(), $this->creditVolumes());
    }

    protected function performances(): Performances
    {
        return new Performances(...$this->validPerformanceParams());
    }

    protected function performancesSummaries(): PerformancesSummaries
    {
        return new PerformancesSummaries(...$this->validPerformancesSummariesParams());
    }

    protected function performancesSummary(): PerformancesSummary
    {
        return new PerformancesSummary($this->performancesSummaries(), $this->amount(), $this->creditVolumes());
    }

    protected function validPerformanceParams(): array
    {
        return $this->arrayOf(fn () => $this->performance());
    }

    protected function validPerformancesSummariesParams(): array
    {
        return $this->arrayOf(fn () => $this->performanceSummary());
    }

    final protected function invalidPerformancesParams(): array
    {
        return $this->arrayOf(fn () => $this->largeValue());
    }

    final protected function invalidPerformancesSummariesParams(): array
    {
        return $this->arrayOf(fn () => $this->largeValue());
    }
}
