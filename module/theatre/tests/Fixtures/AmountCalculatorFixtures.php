<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\AmountCalculator;
use Theatre\AmountRule;
use Theatre\Collection\AmountRules;
use Theatre\ValueObject\Play\Type;

trait AmountCalculatorFixtures
{
    use RandomScalarValuesFixtures;
    use PerformanceFixtures;
    use AmountFixtures;
    use DependenciesFixtures;

    final public function amountCalculatorWithRulesForPlayType(Type $type, AmountRules $amountRules): AmountCalculator
    {
        $amountCalculator = $this->amountCalculator();
        $amountCalculator->addAmountRules($type, $amountRules);

        return $amountCalculator;
    }

    final protected function amountRules(?callable $amountRuleProvider = null): AmountRules
    {
        return new AmountRules(...$this->arrayOf($amountRuleProvider ?? fn () => $this->createMock(AmountRule::class)));
    }
}
