<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\ValueObject\Invoice;

trait InvoiceFixtures
{
    use CustomerFixtures;
    use PerformanceFixtures;

    public function invoice(): Invoice
    {
        return new Invoice($this->customer(), $this->performancesSummary());
    }
}
