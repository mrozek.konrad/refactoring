<?php

declare(strict_types=1);

namespace Theatre\Tests\Fixtures;

use Theatre\Collection\CreditVolumesRules;
use Theatre\CreditVolumesCalculator;
use Theatre\CreditVolumesRule;

trait CreditVolumesCalculatorFixtures
{
    use RandomScalarValuesFixtures;
    use PerformanceFixtures;
    use CreditVolumesFixtures;
    use DependenciesFixtures;

    final public function creditVolumesCalculatorWithRulesForPlayType(\Theatre\ValueObject\Play\Type $type, CreditVolumesRules $creditVolumesRules): CreditVolumesCalculator
    {
        $amountCalculator = $this->creditVolumesCalculator();
        $amountCalculator->addCreditVolumesRules($type, $creditVolumesRules);

        return $amountCalculator;
    }

    final protected function creditVolumesRules(?callable $creditVolumesRuleProvider = null): CreditVolumesRules
    {
        return new CreditVolumesRules(...$this->arrayOf($creditVolumesRuleProvider ?? fn () => $this->createMock(CreditVolumesRule::class)));
    }
}
