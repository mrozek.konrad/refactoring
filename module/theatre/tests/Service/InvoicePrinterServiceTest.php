<?php

declare(strict_types=1);

namespace Theatre\Tests\Service;

use Theatre\InvoicePrinter;
use Theatre\Tests\Fixtures\DependenciesFixtures;
use Theatre\Tests\Fixtures\InvoiceFixtures;
use Theatre\Tests\TheatreTestCase;

final class InvoicePrinterServiceTest extends TheatreTestCase
{
    use InvoiceFixtures;
    use DependenciesFixtures;

    public function testImplementsInterface(): void
    {
        $invoiceCreator = $this->invoicePrinter();

        $this->assertInstanceOf(InvoicePrinter::class, $invoiceCreator);
    }

    public function testPrintsInvoiceInCorrectFormat(): void
    {
        $invoice = $this->invoice();

        $expectedResult = 'Rachunek dla ' . $invoice->customer()->name() . PHP_EOL;
        foreach ($invoice->performancesSummary()->performancesSummaries() as $performancesSummary) {
            $expectedResult .= ' ' . $performancesSummary->performance()->play()->name()->value() .
                               ': ' . \number_format($performancesSummary->amount()->value() / 100, 2) .
                               ' (liczba miejsc: ' . $performancesSummary->performance()->audience()->value() . ')' . PHP_EOL;
        }

        $expectedResult .= 'Naleznosc: ' . \number_format($invoice->performancesSummary()->totalAmount()->value() / 100) . PHP_EOL;
        $expectedResult .= 'Punkty promocyjne: ' . (string) $invoice->performancesSummary()->totalCreditVolumes()->value() . PHP_EOL;

        $result = $this->invoicePrinter()->print($invoice);

        $this->assertSame($expectedResult, $result);
    }
}
