<?php

declare(strict_types=1);

namespace Theatre\Tests\Service;

use Theatre\InvoiceCreator;
use Theatre\PerformancesSummaryCreator;
use Theatre\Tests\Fixtures\CustomerFixtures;
use Theatre\Tests\Fixtures\DependenciesFixtures;
use Theatre\Tests\Fixtures\PerformanceFixtures;
use Theatre\Tests\TheatreTestCase;

final class InvoiceCreatorServiceTest extends TheatreTestCase
{
    use CustomerFixtures;
    use PerformanceFixtures;
    use DependenciesFixtures;

    public function testImplementsInterface(): void
    {
        $invoiceCreator = $this->invoiceCreator();

        $this->assertInstanceOf(InvoiceCreator::class, $invoiceCreator);
    }

    public function testCreatedInvoiceHasValidCustomerAndPerformancesSummary(): void
    {
        $customer     = $this->customer();
        $performances = $this->performances();

        $performancesSummary = $this->performancesSummary();

        $performancesSummaryCreator = $this->createMock(PerformancesSummaryCreator::class);
        $performancesSummaryCreator->expects($this->once())->method('createSummary')->with($performances)->willReturn($performancesSummary);

        $invoiceCreator = $this->invoiceCreator($performancesSummaryCreator);

        $invoice = $invoiceCreator->create($customer, $performances);

        $this->assertSame($customer, $invoice->customer());
        $this->assertSame($performancesSummary, $invoice->performancesSummary());
    }

    public function testUsesPerformancesSummaryCreatorToCreatePerformancesSummary(): void
    {
        $customer     = $this->customer();
        $performances = $this->performances();

        $performancesSummary = $this->performancesSummary();

        $performancesSummaryCreator = $this->createMock(PerformancesSummaryCreator::class);
        $performancesSummaryCreator->expects($this->once())
            ->method('createSummary')
            ->with($performances)
            ->willReturn($performancesSummary);

        $invoiceCreator = $this->invoiceCreator($performancesSummaryCreator);

        $invoiceCreator->create($customer, $performances);
    }
}
