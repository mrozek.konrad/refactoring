<?php

declare(strict_types=1);

namespace Theatre\Tests\Service;

use Theatre\AmountCalculator;
use Theatre\Collection\PerformancesSummaries;
use Theatre\CreditVolumesCalculator;
use Theatre\PerformancesSummaryCreator;
use Theatre\Tests\Fixtures\DependenciesFixtures;
use Theatre\Tests\Fixtures\PerformanceFixtures;
use Theatre\Tests\TheatreTestCase;
use Theatre\ValueObject\PerformancesSummary;

final class PerformancesSummaryCreatorServiceTest extends TheatreTestCase
{
    use PerformanceFixtures;
    use DependenciesFixtures;

    public function testImplementsInterface(): void
    {
        $invoiceCreator = $this->performancesSummaryCreator();

        $this->assertInstanceOf(PerformancesSummaryCreator::class, $invoiceCreator);
    }

    public function testCreatedSummaryHasValidState(): void
    {
        $performances             = $this->performances();
        $calculatedAmount         = $this->amount();
        $creditVolumes            = $this->creditVolumes();

        $expectedTotalCalculatedAmount = $calculatedAmount->multiply(\count($performances));
        $expectedTotalCreditsVolumes   = $creditVolumes->multiply(\count($performances));

        $amountCalculator        = $this->createMock(AmountCalculator::class);
        $creditVolumesCalculator = $this->createMock(CreditVolumesCalculator::class);

        $amountCalculator->expects($this->exactly(\count($performances)))->method('calculate')->willReturn($calculatedAmount);
        $amountCalculator->expects($this->once())->method('calculateTotalAmount')->willReturn($expectedTotalCalculatedAmount);

        $creditVolumesCalculator->expects($this->exactly(\count($performances)))->method('calculate')->willReturn($creditVolumes);
        $creditVolumesCalculator->expects($this->once())->method('calculateTotalCreditVolumes')->willReturn($expectedTotalCreditsVolumes);

        $performancesSummaryCreator = $this->performancesSummaryCreator($amountCalculator, $creditVolumesCalculator);

        $performancesSummary = $performancesSummaryCreator->createSummary($performances);

        $this->assertInstanceOf(PerformancesSummary::class, $performancesSummary);
        $this->assertInstanceOf(PerformancesSummaries::class, $performancesSummary->performancesSummaries());
        $this->assertCount(\count($performances), $performancesSummary->performancesSummaries());
        $this->assertTrue($expectedTotalCreditsVolumes->areEquals($performancesSummary->totalCreditVolumes()));
        $this->assertTrue($expectedTotalCalculatedAmount->areEquals($performancesSummary->totalAmount()));
    }
}
