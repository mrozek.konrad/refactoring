<?php

declare(strict_types=1);

namespace Theatre\Tests\Collection;

use Theatre\Collection\PerformancesSummaries;
use Theatre\Tests\Fixtures\PerformanceFixtures;
use Theatre\Tests\TheatreTestCase;
use Theatre\ValueObject\PerformanceSummary;
use TypeError;

final class PerformancesSummariesTest extends TheatreTestCase
{
    use PerformanceFixtures;

    public function testPerformancesSummariesCollectionCanBeCreatedOnlyUsingObjectsOfPerformanceSummaryType(): void
    {
        $validParams = $this->validPerformancesSummariesParams();

        $performancesSummaries = new PerformancesSummaries(...$validParams);

        $this->assertSame($validParams, $performancesSummaries->getArrayCopy());
        $this->assertSame(\reset($validParams), $performancesSummaries->current());
        $this->assertInstanceOf(PerformanceSummary::class, $performancesSummaries->current());
    }

    public function testPerformancesSummariesCollectionCannotBeCreatedUsingObjectsOfTypeDifferentThanPerformanceSummary(): void
    {
        $invalidParams = $this->invalidPerformancesSummariesParams();

        $this->expectException(TypeError::class);

        new PerformanceSummary(...$invalidParams);
    }
}
