<?php

declare(strict_types=1);

namespace Theatre\Tests\Collection;

use Theatre\Collection\CreditVolumesRules;
use Theatre\CreditVolumesRule;
use Theatre\Tests\Fixtures\CreditVolumesRulesFixtures;
use Theatre\Tests\TheatreTestCase;
use TypeError;

final class CreditVolumesRulesTest extends TheatreTestCase
{
    use CreditVolumesRulesFixtures;

    public function testPerformancesCollectionCanBeCreatedOnlyUsingObjectsOfPerformanceType(): void
    {
        $validParams = $this->validCreditVolumesParams();

        $creditVolumesRules = new CreditVolumesRules(...$validParams);

        $this->assertSame($validParams, $creditVolumesRules->getArrayCopy());
        $this->assertSame(\reset($validParams), $creditVolumesRules->current());
        $this->assertInstanceOf(CreditVolumesRule::class, $creditVolumesRules->current());
    }

    public function testPerformancesCollectionCannotBeCreatedUsingObjectsOfTypeDifferentThanPerformance(): void
    {
        $invalidParams = $this->invalidCreditVolumesParams();

        $this->expectException(TypeError::class);

        new CreditVolumesRules(...$invalidParams);
    }
}
