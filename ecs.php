<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\Whitespace\BlankLineBeforeStatementFixer;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();

    $parameters->set('paths', [__DIR__ . '/module']);

    $parameters->set(
        'sets',
        [
            SetList::ARRAY,
            SetList::COMMON,
            SetList::CLEAN_CODE,
            SetList::PSR_12,
            SetList::SYMFONY,
        ]
    );

    $parameters->set('skip', ['PhpCsFixer\Fixer\Whitespace\MethodChainingIndentationFixer' => null]);

    $services = $containerConfigurator->services();

    $services->set(BlankLineBeforeStatementFixer::class);

    $services->set('PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer')
             ->call('configure', [['syntax' => 'short']]);

    $services->set('PhpCsFixer\Fixer\Operator\BinaryOperatorSpacesFixer')
             ->call('configure', [['default' => 'align']]);

    $services->set('PhpCsFixer\Fixer\Operator\ConcatSpaceFixer')
             ->call('configure', [['spacing' => 'one']]);
};
