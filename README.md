## Refactoring base code from procedural paradigm to object paradigm.

###### Run example:

Base code:

`php application/theatre/runner_procedural.php`

Code after refactoring:

`php application/theatre/runner.php`