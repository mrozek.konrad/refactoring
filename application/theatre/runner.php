<?php

use Theatre\TheatreFacadeFactory;
use TheatreConfiguration\WarsawTheatreConfiguration;

require_once 'vendor/autoload.php';

$theatreConfiguration = new WarsawTheatreConfiguration();
$theatreFacadeFactory = new TheatreFacadeFactory();

$theatreFacade   = $theatreFacadeFactory->create();
$invoices        = $theatreFacade->prepareInvoices($theatreConfiguration);
$invoicesSummary = $theatreFacade->prepareInvoicesTextSummary($invoices);

echo $invoicesSummary;